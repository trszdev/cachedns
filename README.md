# CacheDns
версия питона: 3.6+

### Запуск
`python server.py`

Чтобы сохранялся кэш, нужно выходить через Ctrl+C

### Запросы к серверу (Windows)
`nslookup -type=A e1.ru 127.0.0.1`

### Формат DNS пакетов
http://kunegin.com/ref3/dns/format.htm

### Дамп DNS пакетов для Wireshark
`dump.pcapng`

### Сжатие имени (0xc00c)
https://stackoverflow.com/questions/9865084/dns-response-answer-authoritative-section
